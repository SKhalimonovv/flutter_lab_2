bool isNetworkImage(String imageSrc) {
    return imageSrc.indexOf('http') != -1;
}