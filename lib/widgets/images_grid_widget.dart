import 'package:flutter/material.dart';
import 'package:flutter_lab_2/pages/image_details_page.dart';
import 'package:flutter_lab_2/utils/image_utils.dart';

class ImagesGrid extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ImagesGridState(5);
  }
}

class ImagesGridState extends State<ImagesGrid> {
  final TextEditingController _textFieldController = TextEditingController();

  final String _imagesFolderName = 'resources/avengers';
  final List<String> _images = <String>[];

  ImagesGridState(int numberOfImages) {
    initImages(numberOfImages);
  }

  void initImages(int imagesCount) {
    for (var i = 1; i <= imagesCount; i++) {
      _images.add('$_imagesFolderName/$i.jpg');
    }
  }

  Widget _getImageInk(String imageSrc) {
    final imageProvider = isNetworkImage(imageSrc) 
      ? NetworkImage(imageSrc) 
      : AssetImage(imageSrc);
    return Ink.image(
        image: imageProvider,
        fit: BoxFit.cover,
        child: InkWell(onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute<ImageDetailsPage>(
                builder: (BuildContext context) => ImageDetailsPage(assetPath: imageSrc)
              )
            );
          }),
        );
  }

  void _addNewImage(BuildContext context) {
    showDialog<AlertDialog>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Add new image by URL'),
          content: TextField(
            controller: _textFieldController,
            decoration: InputDecoration(hintText: 'Image URL'),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
              ),
            FlatButton(
              child: Text('Add'),
              onPressed: () {
                setState(() {
                  if (!isNetworkImage(_textFieldController.text)) {
                    return;
                  }

                  if (_textFieldController.text.length > 0) {
                    _images.add(_textFieldController.text);
                    _textFieldController.clear();
                  }
                });
                Navigator.of(context).pop();
              },
            )
          ],
        );
      });
  }

  Widget _buildGrid() {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2
      ),
      itemCount: _images.length,
      itemBuilder: (BuildContext ctxt, int index) {
        final String image = _images[index];
        return _getImageInk(image);
      }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildGrid(),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _addNewImage(context),
        tooltip: 'Add',
        child: Icon(Icons.add),
      )
    );
  }
}