import 'package:flutter/material.dart';
import 'package:flutter_lab_2/utils/image_utils.dart';

class ImageDetailsPage extends StatelessWidget {

  final String assetPath;

  ImageDetailsPage({
    Key key, @required this.assetPath
    }) : assert(assetPath != null),
    super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Details'),),
      body: Center(
        child: _getImage(this.assetPath),)
    );
  }

  Image _getImage(String imagePath) {
    return isNetworkImage(imagePath)
      ? Image.network(imagePath)
      : Image.asset(imagePath);
  }
}